# My Shell Architecture

My Shell is split into *4* sections:

1. Request Input
2. Handle Input
3. Parse Input
4. Execute Command

## 1 - Request Input

#### Single Word Command - (OBSOLETE)
Utilizes **scanf**

#### Multi-Word Command
Utilizes **getline**

## 2 - Handle Input

#### Single-Word Command - (OBSOLETE)
Not needed

#### Multi-Word Command
* Remove trailing *\n* due to getline

## 3 - Parse Input

#### Single-Word Command - (OBSOLETE)
* Compare string to a command -> strcmp(input, command)

#### Multi-Word Command
* Parse command into a list of string
    <command> <option> <args>

## 4 - Execute Command

#### Single-Word Command - (OBSOLETE)
* Change state of shell

#### Multi-Word Command
* Execute command based on *options* and *args*
