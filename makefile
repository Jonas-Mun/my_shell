.PHONY: all say_hello generate clean

CC = gcc

LINKERFLAG = -lm

DIRECTORY = src

all: my_shell

my_shell:
	${CC} ${LINKERFLAG} ${DIRECTORY}/my_hash.c ${DIRECTORY}/utils/read_file.c ${DIRECTORY}/my_shell.c

clean:
	@echo "Cleaning up..."
	rm a.out
