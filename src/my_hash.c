#include <stdio.h>
#include <string.h>
#include <math.h>

#include "my_hash.h"

int my_hash(char *str) {
	int n = strlen(str);
	int hash = 0;
	for (int i = 0; i < n; i++) {
		hash += pow(2,i) * str[i];
	}
	return hash;
}
