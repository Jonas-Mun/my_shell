/**
 * My version of a Shell for experimentation
 *
 * Version: 0.2
 *
 * Receives a line of command to execute.
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "command.h"
#include "my_hash.h"
#include "utils/utils.h"

/**
 * Command Hash Values
 */

// exit
#define EXIT 1689

// read_file -> 
#define RDFILE 52688
// convert_base -> 425913
#define CNVTBASE 425913

/* -----------------*/

size_t MAXBUFFER = 512;
int gl_version = 2;

void print_header();
int parse_input(struct command **start, char buffer[]);
int input_manager(char buffer[]);
int buffer_validation(char buffer[], int max_length);
int execute_command(struct command *com);

int main() 
{
	char buffer[MAXBUFFER];

	print_header();
	while(1) {
		printf("shell > ");

		// input
		// -----
		int success = input_manager(buffer);

		// input error handling
		// --------------------
		if (success == -1) {
			puts("ERROR: EXITING");
			return -1;
		}

		// manage input buffer
		// -------------------
		buffer_validation(buffer, MAXBUFFER);	

		// parse input
		// -----------
		int num_commands;

		printf("[Initializing command structure]\n");
		struct command *start = malloc(sizeof(struct command));
		start->comm = NULL;
		start->options = NULL;
		start->args = NULL;

		num_commands = parse_input(&start,buffer);
		printf("[Finished parsing command]\n");
		printf("[Command] %s\n", start->comm);

		if(start->options == NULL) {
			printf("[!!] No options\n");
		} else {
			printf("%c\n",start->options->character);
		}

		if (start->args == NULL) {
			printf("[!!] No arguments\n");
		} else {
			printf("%s \n",start->args->string);
		}

		execute_command(start);
	}
}

void print_header() {
	printf("* Welcome to My Shell v0.%d\n", gl_version);
	printf("* Only accepts single word commands *\n\n");

}

int input_manager(char buffer[]) {
	
	// gather whole entry
	// ------------------
	getline(&buffer, &MAXBUFFER, stdin);

	// remove trailing '\n'
	// ------------------
	int length = strlen(buffer);
	buffer[length-1] = '\0';

	return 0;
}

int parse_input(struct command **new_command, char buffer[]) {
	struct command *curr_command = *new_command;
	char *token;
	char delim = ' ';
	token = strtok(buffer, &delim);
	// parse command
	// -------------
	int command_length = strlen(token);
	curr_command->comm = malloc(sizeof(char) * command_length);

	strcpy(curr_command->comm, token);

	printf("[Command parsed] %s\n", curr_command->comm);

	struct node *beg_options;
	struct node *beg_args;

	beg_options = curr_command->options;
	
	struct node *cur_option = beg_options;
	struct node *cur_arg = beg_args;
	token = strtok(NULL, &delim);
	while (token != NULL) {
		// parse option
		// ------------
		if (token[0] == '-') {
			if (curr_command->options == NULL) {
				curr_command->options = malloc(sizeof(struct node));
				cur_option = curr_command->options;
			} else {
				struct node *prev_opt = cur_option;
				cur_option = cur_option->next;
				cur_option = malloc(sizeof(struct node));
			}

			cur_option->type = CHARACTER;
			cur_option->next = NULL;
			cur_option->character = token[1];
				
		}
		// parse argument
		// --------------
		else {
			if (curr_command->args == NULL) {
				curr_command->args = malloc(sizeof(struct node));
				cur_arg = curr_command->args;
			}
			else {
				struct node *prev_arg = cur_arg;
				cur_arg = cur_arg->next;
				prev_arg->next = cur_arg;
				cur_arg = malloc(sizeof(struct node));
			}
			cur_arg->type = STRING;
			cur_arg->next = NULL;

			int tok_length = strlen(token);
			cur_arg->string = malloc(sizeof(char) * tok_length);
			strcpy(cur_arg->string, token);
			printf("[Argument parsed] %s\n", token);
		}
		token = strtok(NULL, &delim);
	}
	
	return 0;
}

int buffer_validation(char buffer[], int max) {
	printf("OK\n");
}

int execute_command(struct command *given_command) {
	int hashed_command = my_hash(given_command->comm);	

	int status = 0;

	switch(hashed_command) {
		case RDFILE: 
			printf("Reading file\n");
			/* DUMMY VARAIBLES */
			char ops[1];
			char *argus[1];
			argus[0] = "text.txt";
			read_file(ops, argus);
			break;
		case CNVTBASE:
			printf("Converting value\n");
			break;
		case EXIT:
			printf("Terminating Shell\n");
			status = -1;
			exit(1);
		default:
			printf("Unknown Command\n");
	}

	return status;
}
