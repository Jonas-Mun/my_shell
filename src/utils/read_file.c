#include <stdio.h>

#include "utils.h"

#define DEBUG 0

#if DEBUG

int main() 
{
	char *files[1];
	files[0] = "text.txt";

	char options[1];	
	read_file(options, files);
}

#endif

int read_file(char options[], char *arg[]) {
	FILE *fptr;

	// opening file
	// ------------
	fptr = fopen(arg[0], "r");

	if (fptr == NULL) {
		printf("[!!] Failed to open file: %s\n", arg[0]);
		return -1;
	}

	// read/write file
	// ---------------
	char buffer[256];
	while (fscanf(fptr, "%s", buffer) != EOF) {
		printf("%s\n", buffer);
	}

	// closing file
	// ------------
	fclose(fptr);

}
