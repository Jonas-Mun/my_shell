enum linked_type {CHARACTER, STRING};

int MALLOCED;
int FREED;

struct node {
	enum linked_type type;
	struct node *next;
       	union {
		char character;
		char *string;
	};
};

// DONE: insert_node
// 	 remove_node NEEDS TESTING
// 	 print_list
// 	 destroy_linked_node

// TODO: 
// 	 init_node

void init_node(struct node *null_start, enum linked_type node_type);

int insert_node(struct node *start, char *val);

int remove_node(struct node **start, int index);

int destroy_linked_node(struct node *start);

void print_list( struct node *start);

