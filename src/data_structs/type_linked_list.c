#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "type_linked_list.h"

extern int MALLOCED;
extern int FREED;

struct node *add_next_element(struct node *curr_node, char *val);
void remv_element(struct node **remv_node);

int main() 
{
	struct node *start;
	start = malloc(sizeof(struct node));
	MALLOCED++;
	start->next = NULL;

	start->type = CHARACTER;	
	start->character = 'c';

	char my_char = 'd';
	char sec_char = 'e';

	int insert_status;
	insert_status = insert_node(start, &my_char);
	insert_node(start, &sec_char);

	destroy_linked_node(start);

	printf("%d, %d\n", MALLOCED, FREED);

}

int insert_node(struct node *start, char *val) {
	struct node *curr_node = start;
	// transverse through linked list
	// ------------------------------
	while (curr_node->next != NULL ) {
		curr_node = curr_node->next;	
	}	

	// add next node after end of list
	// -------------------------------
	struct node *added_node = add_next_element(curr_node, val);	
	
	int status = 0;
	// verify added_node
	// -----------------
	if (added_node->type == CHARACTER) {
		if (added_node->character != *val) {
			status = -1;
		}
	} else {
		if (strcmp(added_node->string, val) != 0) {
			status = -1;
		}
	}

	return status;
}

struct node *add_next_element(struct node *curr_node, char *val) {
	// new node
	// --------
	struct node *new_node;
	new_node = malloc(sizeof(struct node));
	MALLOCED++;
	new_node->next = NULL;

	// infer type
	// ------------
	if (curr_node->type == CHARACTER) {
		new_node->type = CHARACTER;
		new_node->character = *val;
	} else {
		new_node->type = STRING;
		// copy chars from val to string
		// -----------------------------
		int length = strlen(val);
		new_node->string = malloc(sizeof(char) * (length + 1));
		MALLOCED++;

		strcpy(new_node->string, val);
	}

	curr_node->next = new_node;
	return curr_node->next;
}

int remove_node(struct node **start, int index) {
	struct node *curr_node = *start;
	struct node *prev_node = NULL;
	// iterate through list
	// --------------------
	for(int i = 0; i < index; i++) {
		prev_node = curr_node;
		curr_node = curr_node->next;
		// out of bounds index
		// -------------------
		if (curr_node == NULL) {
			return -1;
		}
	}

	// remove node
	// -----------
	struct node *remv_node;
	if (prev_node == NULL) {	// first element
		remv_node = curr_node;	
		curr_node = curr_node->next;

		remv_element(&remv_node);

		*start = curr_node;
	} else {
		struct node *temp = curr_node;
		prev_node->next = curr_node->next;	// remove current node from chain

		remv_element(&temp);
	}

	return 0;
}

void remv_element(struct node **remv_node) {
	struct node *curr_node = *remv_node;
	// check type
	// ----------
	if (curr_node->type == STRING) {
		free(curr_node->string);
		FREED++;
	}
	free(curr_node);
	FREED++;
}

void print_list(struct node *start) {
	struct node *curr_node = start;
	while(curr_node != NULL) {
		if(curr_node->type == CHARACTER) {
			printf("%c\n", curr_node->character);
		} else {
			printf("%s\n", curr_node->string);
		}
		curr_node = curr_node->next;
	}
}

int destroy_linked_node(struct node *start) {
	struct node *curr_node = start;
	struct node *temp_node;

	// iterate through nodes
	// ---------------------
	while(curr_node->next != NULL) {
		temp_node = curr_node;
		curr_node = curr_node->next;	

		remv_element(&temp_node);
	}
	remv_element(&curr_node);	// last element

	return 0;
}
