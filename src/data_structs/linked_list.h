struct linked_node {
	int val;
	struct linked_node * next;
};

int insert_node(struct linked_node * start, int val);

int remove_node(struct linked_node ** start, int index);

int destroy_linked_node(struct linked_node * start);

void print_list(struct linked_node * start);
