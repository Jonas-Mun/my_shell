#include <stdio.h>
#include <stdlib.h>

#include "linked_list.h"

//Functions tested:
// insert
// remove_node
// destroy_linked_node
// print_list
//

extern int malloced_mem;
extern int freed_mem;

int main() {
	printf("Hello!");
	printf("%d, malloced meme\n", malloced_mem);

	// Initialize list
	// ---------------
	struct linked_node  *start = malloc(sizeof(struct linked_node));
	malloced_mem++;

	start->val = 3;
	start->next = NULL;

	// test functions
	// --------------
	insert_node(start, 4);
	print_list(start);

	insert_node(start, 6);
	print_list(start);

	remove_node(&start, 0);
	print_list(start);
	

	// clean up
	// --------
	destroy_linked_node(start);


	// debug memory
	// ------------
	printf("\n%d, %d\n", malloced_mem, freed_mem);
	if (malloced_mem == freed_mem) {
		printf("All allocated memory freed\n");
		return 0;
	}
	else {
		printf("Some memory not de-allocated\n");
		return -1;
	}
}
