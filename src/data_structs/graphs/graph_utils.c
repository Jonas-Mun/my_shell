#include "graph.h"
#include <stdio.h>
#include <string.h>

/**
 * Utilizes Breadth First Search
 *
 *
 */
int is_reachable(struct Graph *g, char *root, char *n) {
	// find root - TESTED
	// ---------
	struct vertex *v = g->vertices;
	while (v != NULL) {
		if (strcmp(v->name, root) == 0) {
			break;
		}
		v = v->next;
	}

	if (v == NULL) {	// root not found
		printf("Root not found\n");
		return 0;
	} else {
		printf("Found Root\n");
	}
	// directs to itself - TESTED
	// -----------------
	if (strcmp(v->name, n) == 0) {
		printf("Directs to itself\n");
		return 1;
	} else {
		printf("Does not direct to itself\n");
	}

	// prepare queue -> Caused segmentation falt due to not initializing the pointers to NULL. Was utilizing memory that contained data. Creating Undefined Behaviour.
	// -------------
	struct v_Queue que;
	que.head = NULL;
	que.end = NULL;
	add_Queue(&que, v);	// initialize algorithm

	if ((que.head->v) == v && (que.end->v) == v) {
		printf("Queue has been initialized\n");
	} else {
		printf("Queue has not been initialized\n");
	}

	// find a path
	// -----------
	while (que.head != NULL) {
		struct vertex *cur_v = pop_Queue(&que);
		struct Edges *es = cur_v->edges;

		while (es != NULL) {
			printf("Vertex %s\n", es->vert->name);
			if (strcmp(es->vert->name, n) == 0) {
				return 1;
			}
			add_Queue(&que, es->vert);
			es = es->next;
			
		}
	} 
	return 0;
		
}
