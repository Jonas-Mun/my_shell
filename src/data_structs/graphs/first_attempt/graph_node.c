#include <stdio.h>
#include <stdlib.h>

#include "graph_node.h"

#define DEBUG 1

//TODO: 
//		display_graph
	
/**
 * To Test
 * 	init_graph (DONE)
 * 	insert_graph (DONE)
 * 	remove_graph_node (DONE)
 * 	destroy_graph
 */

#if DEBUG

int test_insert_node(struct list_vertex *graph, char label, int index);
int test_init_graph(struct list_vertex *graph);
int test_remv_node(struct list_vertex *graph, char label);

int main()
{
	struct list_vertex *graph_list = init_graph();
	if (graph_list == NULL) {
		printf("[!!] Error: Could not allocate memory\n");
		exit(1);
	}

	int test_status = 0;


	// Testing 'init_graph'
	// --------------------
	printf("[Testing] init_graph\n");
	test_status = test_init_graph(graph_list);
	if (test_status == -1) {
		exit(1);
	}

	// Testing 'insert_graph_node'
	// ---------------------------
	insert_graph_node(graph_list, 'A');

	printf("[Testing] insert_graph\n");
	test_status = test_insert_node(graph_list, 'A', 0);
	if (test_status == -1) {
		exit(1);
	}

	// Testing 'remove_graph_node'
	// ---------------------------	
	remove_graph_node(&graph_list, 'A');

	printf("[Testing]  remv_graph_node\n");
	test_status = test_remv_node(graph_list, 'A');
	if (test_status == -1) {
		exit(1);
	}

	insert_graph_node(graph_list, 'B');

	test_status = test_insert_node(graph_list, 'B', 0);
}

int test_init_graph(struct list_vertex *graph) {
	if (graph->node == NULL && graph->next == NULL) {
		printf("[+] init_graph() success\n");	
	} else {
		printf("[-] init_graph() FAILED\n");
		return -1;
	}
	return 1;
}

int test_insert_node(struct list_vertex *graph, char label, int index) {
	// find node
	// ---------
	for (int i = 0; i < index; i++) {
		if (graph->node == NULL) {
			printf("[!!] Outside graph boundary: reduce index if possible\n");
			return -1;
		}
		graph = graph->next;
	}

	if (graph->node->label == label) {
		printf("[+] insert_graph_node() success\n");
		return 1;
	} else {
		printf("[-] insert_graph() FAILED\n");
		return -1;
	}
}

int test_remv_node(struct list_vertex *graph, char label) {
	while (graph->node != NULL) {
		if (graph->node->label == label) {
			printf("[-] remv_node FAILED\n");
			return -1;
		}
		graph = graph->next;
	}
	printf("[+] remv_node success\n");
	return 1;
}
#endif

struct list_vertex *init_graph() {
	struct list_vertex *list = malloc(sizeof(struct list_vertex *));
	list->node = NULL;	// Set first node to null;
	list->next = NULL;

	return list;
}

/** 
 * insert_graph_node 
 *
 * Allocates a new node in the heap, and inserts it in the graph
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 */
int insert_graph_node(struct list_vertex *start, char node_name) {
	// create new node
	// --------------
	struct vertex *new_node = malloc(sizeof(struct vertex));
	if (new_node == NULL) {
		printf("[!!] Error: Could not allocate memory for new node\n");
		return -1;
	}	

	new_node->label = node_name;
	new_node->connections = NULL;

	// add new node
	// ------------
	if (start->node == NULL) {	// first node
		start->node = new_node;
		start->next = NULL;
	} else {
		// transverse to end of list
		// -------------------------
		while (start->next != NULL) {
			start = start->next;
		}
		// create link
		// -----------
		struct list_vertex *new_link = malloc(sizeof(struct list_vertex *));
		new_link->node = new_node;
		new_link->next = NULL;
		start->next = new_link;
	}

	return 0;
}

void remove_graph_node(struct list_vertex **start, char node_to_remove) {
	struct list_vertex *curr_link = *start;
	struct list_vertex *prev_link = NULL;

	// find the node to remove
	// -----------------------
	while (curr_link != NULL) {
		if (curr_link->node->label == node_to_remove) {
			printf("[remove_graph_node] Node found.\n");
			break;
		} 		
		prev_link = curr_link;
		curr_link = curr_link->next;
	}

	if (curr_link == NULL) {
		printf("[!!] Node does not exist in graph\n");
	} else if (prev_link == NULL) {	// one node in graph
		free(curr_link->node->connections);
		curr_link->node->connections = NULL;
		free(curr_link->node);
		curr_link->node = NULL;

	} else {
		// unlink node to be removed
		// -------------------------
		struct list_vertex *rmv_link = curr_link;
		curr_link = curr_link->next;
		prev_link->next = curr_link;

		// clean node & link
		// -----------------
		free(rmv_link->node->connections);
		free(rmv_link->node);
		free(rmv_link);
	}
}

/**
 * destroy_graph
 *
 * De-allocates both vertex and struct list from the graph. 
 * Points the start of the graph to NULL. (Hence the **start)
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 */

int destroy_graph(struct list_vertex **start) {
	struct list_vertex *curr = *start;	// save beginning
	// remove list_nodes
	// -------------------------
	while (curr != NULL) {
		struct list_vertex *temp = curr;
		curr = curr->next;

		// remove graph node
		// -----------------
		free(temp->node->connections);
		free(temp->node);
		free(temp);
	}	
	*start = NULL;
	return 1;
}

int display_graph(struct list_vertex *start) {
	while (start != NULL) {
		printf("%c\n");
		

	}
}
