struct vertex {
	char label;
	char *connections;
};

struct list_vertex {
	struct vertex *node;
	struct list_vertex *next;
};

struct list_vertex *init_graph();

int destroy_graph(struct list_vertex **start);

int insert_graph_node(struct list_vertex *start, char new_node);

void remove_graph_node(struct list_vertex **to_graph, char node_to_remove);

int display_graph(struct list_vertex *start);
