#ifndef GRAPH_H
#define GRAPH_H

struct vertex {
	char *name;
	int visited;
	struct Edges *edges;
	struct vertex *next;
};

struct Edges {
	struct vertex *vert;
	struct Edges *next;
};

struct Graph {
	struct vertex *vertices;
};

/* Data structures */

struct v_Queue {
	struct v_List *head;
	struct v_List *end;
};

struct v_List {
	struct vertex *v;
	struct v_List *next;
};

/* Setup / Cleanup */
struct Graph *graph_create();

void graph_destroy(struct Graph **g);
void destroy_vertex(struct vertex *v);
void destroy_edge(struct Edges *e);

/* General Operations */
void graph_add_vertex(struct Graph *g, char *name);
struct vertex *create_vertex(char *name);

void graph_add_edge(struct Graph *g, char *origin, char *dest);
struct Edges *create_edge(struct vertex*v);

int is_reachable(struct Graph *g, char *start, char *dest);

void display_graph(struct Graph *g);
void display_edges(struct vertex *v);

/* Vertex Queue functions */

int add_Queue(struct v_Queue *queue, struct vertex *v);
struct vertex *pop_Queue(struct v_Queue *queue);

#endif
