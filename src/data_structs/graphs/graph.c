#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "graph.h"
#include "graph_utils.h"

#define DEBUG 1

#if DEBUG

/*TODO:
 * 		Test add_Queue
 * 		Test pop_Queue
 */

int main() {
	struct Graph *graph = graph_create();

	graph_add_vertex(graph, "A");
	graph_add_vertex(graph, "B");

	graph_add_edge(graph, "A", "B");

	int reachable = is_reachable(graph, "A", "B");
	if (reachable == 1) {
		printf("Is reachable\n");
	} else {
		printf("Not reachable\n");
	}

	display_graph(graph);
	graph_destroy(&graph);

	if (graph == NULL) {
		printf("Graph has been destroyed\n");
	}

	/** Testing Queue */
	struct vertex *v1 = create_vertex("A");
	struct vertex *v2 = create_vertex("B");

	struct v_Queue *que = malloc(sizeof(struct v_Queue));
	que->head = NULL;
	que->end = NULL;
		
	add_Queue(que, v1);

	if (que->head == que->end) {
		printf("Queue initialized\n");
	}

	add_Queue(que, v2);
	
	struct vertex *new_v = pop_Queue(que);
	if (!(new_v == v1)) {
		printf("ERROR\n");
	}

	new_v = pop_Queue(que);

	if (!(new_v == v2)) {
		printf("ERROR\n");
	}

	if (que->head == NULL) {
		printf("Finished\n");
	}

	return 0;
}

#endif

/** Data Structures **/
int add_Queue(struct v_Queue *q, struct vertex *v) {

	// memory for new vertex in queue
	// ------------------------------
	struct v_List *new_v = malloc(sizeof(struct v_List));
	new_v->v = v;
	new_v->next = NULL;

	// Add to queue
	// ------------------
	if ( q->head == NULL) {	// empty

		q->head = new_v;
		q->end = new_v;
	} else {
		q->end->next = new_v;	// append new vertex to end of list
		q->end = q->end->next;	// update queue to point to new end
	}

	return 1;
}

struct vertex *pop_Queue(struct v_Queue *q) {
	if (q->head != NULL) {

		struct v_List *v_popped = q->head;

		q->head = v_popped->next;

		struct vertex *v = v_popped->v;
		free(v_popped);

		return v;
	}

	return NULL;
}

/*** Queue Tests ***/


/** Setup / Cleanup **/

struct Graph *graph_create() {
	struct Graph *graph = malloc(sizeof(struct Graph));
	graph->vertices = NULL;

	return graph;
}

void graph_destroy(struct Graph **g) {
	struct vertex *vs = (*g)->vertices;
	while (vs != NULL) {
		struct vertex *prev_v = vs;
		vs = vs->next;

		destroy_vertex(prev_v);
	}
	*g = NULL;
}

/* graph_destroy Helper functions */
void destroy_vertex(struct vertex *v) {
	destroy_edge(v->edges);
	free(v);
}

void destroy_edge(struct Edges *e) {
	while (e != NULL) {
		struct Edges *prev_e = e;
		e = e->next;
		free(prev_e);
	}
}

/* General Operations */

void graph_add_vertex(struct Graph *g, char *vname) {
	struct vertex *new_v = create_vertex(vname);

	// empty graph
	// -----------
	if (g->vertices == NULL) {
		g->vertices = new_v;
	} else {
		struct vertex *vs = g->vertices;

		// go to end of vertices
		// ---------------------
		while (vs->next != NULL) {
			vs = vs->next;
		}
		vs->next = new_v;	// append new vertices
	}
}

/* graph_add_vertex - Helper functions */
struct vertex *create_vertex(char *name) {
	struct vertex *v = malloc(sizeof(struct vertex));

	// copy name to vertex
	// -------------------
	int name_length = strlen(name);
	v->name = malloc(sizeof(char) * (name_length + 1));
	strcpy(v->name, name);

	v->visited = 0;
	v->edges = NULL;
	v->next = NULL;

	return v;
}

void graph_add_edge(struct Graph *g, char *origin, char *dest) {
	struct vertex *vs = g->vertices;

	struct vertex *v_origin = NULL;
	struct vertex *v_dest = NULL;
	// find origin && dest
	// -----------
	while (vs != NULL) {

		// Have we found origin and dest?
		// -----------------------------
		if (v_origin != NULL && v_dest != NULL) {
			break;
		}

		if (strcmp(vs->name, origin) == 0) {	// found origin
			v_origin = vs;
		} else if (strcmp(vs->name, dest) == 0) { 	// found destination
			v_dest = vs;
		}
		vs = vs->next;	// next vertex
	}

	// append edge
	// -----------
	if (v_origin == NULL || v_dest == NULL) {
		printf("Vertices do not exist to create an edge\n");
	}
	else {
		struct Edges *new_e = create_edge(v_dest);
		if (v_origin->edges == NULL) {
			v_origin->edges = new_e;
		} else {
			struct Edges *es = v_origin->edges;

			// go to end of edges
			// ------------------
			while (es->next != NULL) {
				es = es->next;
			}
			es->next = es;
		}
	}
}

/* graph_add_edge - Helper function */
struct Edges *create_edge(struct vertex *v) {
	struct Edges *e = malloc(sizeof(struct Edges));

	e->vert = v;
	e->next = NULL;

	return e;
}

void display_graph(struct Graph *g) {
	struct vertex *vs = g->vertices;
	while ( vs != NULL) {
		printf("%s\n", vs->name);
		display_edges(vs);
		vs = vs->next;
	}
}

void display_edges(struct vertex *v) {
	struct Edges *es = v->edges;
	while (es != NULL) {
		if (es->vert != NULL) {
			printf("->%s\n", es->vert->name);
		}
		es = es->next;
	}	
}
