#include <stdio.h>
#include <stdlib.h>

#include "linked_list.h"

int malloced_mem = 0;
int freed_mem = 0;

int insert_node(struct linked_node * start, int val) {
	// argument handling
	// -----------------
	if (start == NULL) {
		return -1;
	}

	// go to end of list
	// ---------------
	while (start->next != NULL) {
		start = start->next;
	}

	// add new value
	// -------------
	if (start->next == NULL) {
		struct linked_node * next_val;
		next_val = malloc(sizeof(struct linked_node));
		malloced_mem++;

		if (next_val == NULL) {
			return -1;
		}

		next_val->val = val;
		next_val->next = NULL;
		start->next = next_val;
	}

	return 0;
}

/* TODO: */
int remove_node(struct linked_node ** start, int index) {
	// input handling
	// --------------
	if (*start == NULL) {
		printf("ERROR: Received empty list\n");
		return -1;
	}

	struct linked_node * node = *start;
	struct linked_node * prev = NULL;

	// transverse list until index
	// ---------------------------
	for(int i = 0; i < index; i++) {
		prev = node;
		node = node->next;
	}

	// single node
	// -----------
	if (prev == NULL && node->next == NULL) {
		printf("Single node, initializing value to -1");
		node->val = -1;
		// Not best solution
		return 1;
	}

	// multiple nodes
	// --------------
	if (prev == NULL) {	// first element
		*start = node->next;
		free(node);
		freed_mem++;

	} else {
		struct linked_node * next = node->next;
		prev->next = next;

		free(node);
		freed_mem++;
	}

	return 0;
}

int destroy_linked_node(struct linked_node * start) {
	struct linked_node * node = start;
	// transverse list
	// ---------------
	while (node->next != NULL) {
		struct linked_node * ftmp;	// store previous node
		ftmp = node;

		node = node->next;	// move up one node

		free(ftmp);	// free node left behind
		freed_mem++;
	}
	free(node);	// last node
	freed_mem++;
	return 0;

}

void print_list(struct linked_node * start) {
	struct linked_node * node = start;
	// transverse list
	// ---------------
	while (node != NULL) {
		printf("%d ", node->val);
		node = node->next;
	}
	printf("\n");

}
