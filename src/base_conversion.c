#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include "base_conversion.h"

#define DEBUG 1

int base_char_to_dec(char val) {
	char char_values[] = {'a','b','c','d','e','f'};
	for (int i = 0; i < 6; i++) {
		if (char_values[i] == val) {
			int val_represented = 10 + i;
			if (DEBUG) {
				printf("%d\n", val_represented);
			}
			return val_represented;
		}
	}
	return -1;
}

void base_conver(char *numbers, int base) {
	int length = strlen(numbers);

	int result = 0;
	for (int i = 0; i < length ; i++) {
		int val;
		// convert chars to decimals
		// --------------
		if (isdigit(numbers[i])) {
			if (DEBUG) {
				printf("Is digit: %c\n", numbers[i]);
			}
			// char to int
			val = numbers[i] - '0';
		} else {
			if (DEBUG) {
				printf("Is character\n");
			}
			val = base_char_to_dec(numbers[i]);
		}

		// apply equation
		// --------------
		int radix_index = (length-1) - i;
		int base_multi = pow(base,radix_index);
		result = result + (val * base_multi);
		if (DEBUG) {
			printf("Base multi: %d\n", base_multi);
			printf("Result: %d\n", result);
		}
	}

	printf("%d\n",result);
}
